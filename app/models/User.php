<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
//use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Eloquent
{
	//use SoftDeletes;
	protected $table="users";
	protected $fillable=['user_name','password','user_address','user_email','user_ip'];



	public function getList()
	{
		return User::where('status_soft_delete',1)->get();
	}
	public function insertData($value,$id='')
	{
		return User::updateOrCreate($value,['user_id' => $id]);
	}
	public function getUserSoftDelete($id)
	{
		return User::where('user_id',$id)->update(['status_soft_delete'=>'0']);
	}
	
}