<?php
//composer autoload file
define('BASE_URL','http://localhost/mvc/public/');
require_once '../vendor/autoload.php';
use \Firebase\JWT\JWT;
require_once 'database.php';
require_once 'core/App.php';
require_once 'core/Controller.php';
