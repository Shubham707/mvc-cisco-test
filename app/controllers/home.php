<?php 
use Illuminate\Database\Eloquent\Model;
class Home extends Controller
{
	protected $user;
	public function __construct()
	{
		$this->user=$this->model('User');
	}
	public function index()
	{
		
		$this->view('home/index');
	}
	public function create()
	{
		$rand=rand(1000,9999);
		$data=[
			'user_name'=>'Shubham Sahu',
			
			'user_address'=>'kalyan Maharastra',
			'user_email'=>'shubhamsahu707@gmail.com',
			'user_ip'=> $_SERVER['SERVER_ADDR'],
		];
		$count=count($data);
		for($i=0;$i<= $count; $i++){
			$data=['password'=> sha1($rand)];
	    $user=$this->user->create($data);
		}
		if($user!=''){
			echo json_encode(['msgs'=>'User Created successfully!','status'=>true]);
		}else{
			echo json_encode(['msgs'=>'User Created not successfully!','status'=>false]);
		}

	}
	public function uniquePage()
	{
		$this->view('home/unique');
	}
	public function uniqueNo()
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $charactersLength; $i++) {
	    	echo "<tr><td>".mt_rand(10000,99999)."</td><td>".
	         $randomString.= $characters[rand(0, $charactersLength - 1)].'</td></tr>';
	    }
	   
	}
	public function getUser()
	{
		$users=$this->user;
		$user=$users->getList();
		foreach ($user as  $value) {
                echo '<tr><td>'.$value->user_name.'</td>
                <td>'.$value->user_email.'</td>
                <td>'.$value->user_address.'</td>
                <td>'.$value->user_ip.'</td>
                <td>'.$value->user_name.'</td>
                <td><a href="javascript:void(0)" onclick="softDelete('.$value->user_id.');">delete</a></td>
                </tr>';
		}
		
	}
	public function softDelete($id)
	{
		$users=$this->user;
		$user=$users->getUserSoftDelete($id);
		if($user!='')
		{
			echo "User Deleted Successfully";
		}
		else {
			echo "User not deleted!";
		}
	}
	public function figures()
	{
		$this->view('home/figure');
	}
	public function telnet()
	{
		$this->view('home/telnet');
	}
	public function createZip()
	{
		$this->view('home/zip');
	}
	public function uploadFile()
	{
		$destination_path = getcwd().DIRECTORY_SEPARATOR;
		//$dir=BASE_URL.'zipFolder';
		$file=uniqid().$_FILES['uploadgip']['name'];
		$path = $destination_path.'zipFolder/'.$file;
		move_uploaded_file($_FILES['uploadgip']['tmp_name'], $path);
		$unzip = new ZipArchive;
		$out = $unzip->open($file);
		@$unzip->extractTo($path);
		@$unzip->close();
		echo "$file extracted to $path";
	}
	
}