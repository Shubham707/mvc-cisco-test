<!DOCTYPE html>
<html>
<head>
	<title>Users Records</title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
</head>
<body>
<div class="container">
	<div class="row">
 <div class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapibleMenu">
                        
                </div>
                <div class="collapse navbar-collapse" id="collapibleMenu">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo BASE_URL?>home">Home</a></li>
                        <li><a href="<?php echo BASE_URL?>home/uniquePage">Unique No</a></li>
                        <li><a href="<?php echo BASE_URL?>home/figures">Figures</a></li>
                        <li><a href="<?php echo BASE_URL?>home/telnet">Telnet</a></li>
                        <li><a href="<?php echo BASE_URL?>home/createZip">Zip Upload</a></li>
                    </ul>
                    
                </div>
            </div>
        </div>
<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Full Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>IP Address</th>
                <th>Start date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="result">
        	
        </tbody>
        <tfoot>
            <tr>
                <th>Full Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>IP Address</th>
                <th>Start date</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    	$(document).ready(function() {
		    $('#example').DataTable();
		} );
		$.ajax({
			url: 'http://localhost/mvc/public/home/getUser',
			type: 'GET',
			success:function(result){
				$('#result').html(result);
			}
		});
		function softDelete(argument) {
			$.ajax({
			url: 'http://localhost/mvc/public/home/softDelete/'+argument,
			type: 'GET',
			success:function(result){
				alert(result);
				location.reload();
			}
		});
		}
		
    </script>
</div>
</div>
</body>
</html>
